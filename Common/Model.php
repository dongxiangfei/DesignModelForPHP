<?php

namespace Common;

class Model
{
    protected $observers = [];

    function __construct()
    {
        $name = strtolower(str_replace('App\Model\\', '', get_class($this)));
//        var_dump(Application::getInstance()->config['model'][$name]['observer']);
        if (Application::getInstance()->config['model'][$name]['observer']) {
            $observers = Application::getInstance()->config['model'][$name]['observer'];
            foreach ($observers as $class) {
                $this->observers[] = new $class;
            }
        }

    }

    function notify($event)
    {
//        var_dump($this->observers);
        foreach ($this->observers as $observer) {
            $observer->update($event);
        }
    }
}