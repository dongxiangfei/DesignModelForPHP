<?php

namespace Common;

class Loader
{

    /**
     * 自动加载BASEDIR下每个子目录命名空间
     * @param $class
     */
    public static function autoload($class)
    {
        require BASEDIR . '/' . str_replace('\\', '/', $class) . ".php";
    }
}