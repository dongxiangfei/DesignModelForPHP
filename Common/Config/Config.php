<?php
namespace Common\Config;

/**
 * Class Config
 * @package Common\Config
 */
//实现ArrayAccess可以让对象可以像数组一样key/value的形式来使用
class Config implements \ArrayAccess {

    protected $path;
    protected $configs = [];

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Whether a offset exists 检测数组的key是否存在
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->configs[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if(empty($this->configs[$offset])){
            $file_path = $this->path.'/'.$offset.'.php';
            $config = require $file_path;
            $this->configs[$offset] = $config;
        }
        return $this->configs[$offset];
    }

    /**
     * Offset to set 设置数组的key
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        throw new \Exception("cannot write config file.");
    }

    /**
     * Offset to unset 删除数组的key
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->configs[$offset]);
    }
}