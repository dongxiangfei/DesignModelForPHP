<?php

namespace Common;

class Database
{
    //保存类唯一实例的私有静态成员变量
    private static $instance;

//    private static $conn;

    //私有构造函数
    private function __construct()
    {
//        self::$conn = mysqli_connect('localhost','root','');
    }

    //私有克隆函数，防止对象被复制
    private function __clone()
    {
        trigger_error('Clone is not allowed');
    }

    //防止反序列化后创建对象
    public function __wakeup()
    {
        trigger_error('Unserialized is not allowed');
    }

    //公有获取访问实例的静态方法
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
            return self::$instance;
        } else {
            return self::$instance;
        }
    }

//    /**
//     * @return false|\mysqli
//     */
//    public static function getConn()
//    {
//        return self::$conn;
//    }
}