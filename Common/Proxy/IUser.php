<?php
namespace Common\Proxy;


interface IUser
{
    public function getName($id);

    public function setName($id,$name);
}