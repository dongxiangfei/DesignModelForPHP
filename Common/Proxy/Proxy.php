<?php
namespace Common\Proxy;

use Common\Factory;

/**
 * Class Proxy  代理实现mysql读写分离，测试例子，实际中要用Database目录下的代理类
 * @package Common\Proxy
 */
class Proxy implements IUser {

    /**
     * 读
     * @param $id
     */
    public function getName($id)
    {
        $db = Factory::getDatabase('slave');
        $db->query("select name from user where id=$id limit 1");
    }

    /**
     * 写
     * @param $id
     * @param $name
     */
    public function setName($id, $name)
    {
        $db = Factory::getDatabase('master');
        $db->query("select name from user where id=$id limit 1");
    }
}