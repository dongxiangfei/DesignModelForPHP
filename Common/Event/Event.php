<?php
namespace Common\Event;

abstract class Event{

    /**
     * @var array 观察者数组
     */
    private $observers = [];

    /**
     * 增加观察者
     */
    public function addObserver(Observer $observer){
        $this->observers[] = $observer;
    }

    /**
     * 通知事件自动更新
     */
    public function notify(){
        foreach ($this->observers as $observer){
            $observer->update();
        }
    }
}