<?php
namespace Common\Event;

interface Observer{

    public function update($event_info = null);
}