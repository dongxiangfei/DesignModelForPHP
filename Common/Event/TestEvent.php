<?php
namespace Common\Event;

class TestEvent extends Event{

    /**
     * 触发事件
     */
    public function trigger(){
        echo "Event<br/>\n";
        $this->notify();
    }
}