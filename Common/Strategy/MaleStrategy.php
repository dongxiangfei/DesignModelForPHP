<?php
namespace Common\Strategy;

class MaleStrategy implements Strategy {

    public function ad()
    {
        echo "高性能电脑";
    }

    public function category()
    {
        echo "电子产品";
    }
}