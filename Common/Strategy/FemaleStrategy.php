<?php
namespace Common\Strategy;

class FemaleStrategy implements Strategy {

    public function ad()
    {
        echo "2020年新款镂空蕾丝";
    }

    public function category()
    {
        echo "女装";
    }
}