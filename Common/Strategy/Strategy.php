<?php
namespace Common\Strategy;

interface Strategy{

    public function ad();

    public function category();
}
