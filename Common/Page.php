<?php
namespace Common;

use Common\Strategy\Strategy;

class Page{

    /**
     * @var 当前策略
     */
    protected $strategy;

    /**
     * 实际执行的业务代码
     */
    public function index(){
        $this->strategy->category();
        echo '/';
        $this->strategy->ad();

    }

    /**
     * 实现依赖注入
     * @param Strategy $strategy
     */
    public function setStrategy(Strategy $strategy){
        $this->strategy = $strategy;
    }
}