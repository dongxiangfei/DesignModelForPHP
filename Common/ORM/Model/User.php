<?php
namespace Common\ORM\Model;

use Common\Database\MysqliAdapter;

class User{

    public $id;
    public $name;
    public $mobile;
    public $regtime;

    protected $db;

    public function __construct($id)
    {
        $this->db = new MysqliAdapter();
        $this->db->connect('192.168.10.10','homestead','secret','test');
        //对象的读取
        $res = $this->db->query("select * from user where id=".$id." limit 1");
        $data = $res->fetch_assoc();
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->mobile = $data['mobile'];
        $this->regtime = $data['regtime'];
    }


    /**
     * 在对象销毁的时候自动调用析构方法
     */
    public function __destruct()
    {
        $this->db->query("update user set name='{$this->name}', mobile='{$this->mobile}',regtime='{$this->regtime}' where id = {$this->id}");
//        $this->db->close();
    }
}