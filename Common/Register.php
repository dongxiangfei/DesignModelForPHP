<?php

namespace Common;

class Register
{

    //全局注册树、容器
    private static $container;

    public static function set($alias, $object)
    {
        self::$container[$alias] = $object;
    }

    public static function _unset($alias)
    {
        unset(self::$container[$alias]);
    }

    public static function get($alias)
    {
        return isset(self::$container[$alias]) ? self::$container[$alias] : '';
    }
}