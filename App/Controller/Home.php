<?php
namespace App\Controller;
use Common\Controller;
use Common\Factory;

class Home extends Controller
{
    function index()
    {
        $model = Factory::getModel('User');

        //新进员工需要做的流程，全部都用观察者模式来实现
        //当触发User模型的create方法时，调用notify来通知各部门进行相关动作
        $userid = $model->create(array('name' => 'rango', 'mobile' => '189xxxx'));
        return array('userid' => $userid, 'name' => 'rango');
    }

    //要把装饰器先注释掉
    function index2()
    {
        $db = Factory::getDatabase();
        $db->query("select * from user");
        $db->query("update user set name='rango2' where id=1");
//        $db->query("delete from user where id=1");
    }
}