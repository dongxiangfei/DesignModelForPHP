<?php
namespace App\Decorator;


//访问url   http://xx.com/home/index/?app=json
class Json
{
    function beforeRequest($controller)
    {

    }

    function afterRequest($return_value)
    {
        if ($_GET['app'] == 'json')
        {
            echo json_encode($return_value);
        }
    }
}