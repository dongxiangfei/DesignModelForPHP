<?php

use Common\Strategy\MaleStrategy;

define('BASEDIR', __DIR__);
include BASEDIR . "/Common/Loader.php";
spl_autoload_register("\\Common\\Loader::autoload");

////单例模式创建实例
//$db = Common\Database::getInstance();
//
//var_dump($db);
////需要在初始化的时候把注册器的别名等注册好
//Common\Register::set('db1', $db);
////注册器模式+单例模式+工厂模式
////在整个框架中都可以使用，而不必再使用单例模式来创建
//$db1 = Common\Register::get('db1');
//var_dump($db1);
//var_dump($db1 instanceof $db);

//适配器模式
//$db = new Common\Database\PdoAdapter();
//$db->connect('192.168.10.10','homestead','secret','test');
//$result = $db->query("show databases");
//foreach ($result as $item){
//    var_dump($item);
//}
//$db->close();

//策略模式
//$page = new Common\Page();
//if(isset($_GET['male'])){
//    $strategy = new Common\Strategy\MaleStrategy();
//}else{
//    $strategy = new Common\Strategy\FemaleStrategy();
//}
////Page需要strategy的依赖注入
//$page->setStrategy($strategy);
//$page->index();

//ORM
//$user = new Common\ORM\Model\User(1);
////var_dump($user->id,$user->name,$user->mobile,date('Y-m-d',$user->regtime));
//$user->mobile = '18233334444';
//$user->name = 'test';
//$user->regtime = time();

//观察者模式
//$event = new Common\Event\TestEvent();
//$obs = new Common\Event\Observer1();
//$event->addObserver($obs);
//$event->trigger();

//装饰器模式

//遍历/迭代器模式
//$users = new Common\ORM\Model\AllUser();
//foreach ($users as $user){
//    var_dump($user->name);
//    $user->regtime = time();
//}

//代理模式
//$proxy = new Common\Proxy\Proxy();
//$proxy->getName(1);
//$proxy->setName(1,'xx');
//$proxy->getName(1);

//自动加载配置
//$config = new Common\Config\Config(__DIR__.'/Configs');
//var_dump($config['controller']);

//echo '<meta http-equiv="content-type" content="text/html;charset=utf-8">';

Common\Application::getInstance(__DIR__)->dispatch();

