<?php

return [
    'master' => [
        'type'     => 'MySQL',
        'host'     => '192.168.10.10',
        'user'     => 'homestead',
        'password' => 'secret',
        'dbname'   => 'test',
    ],
    'slave'  => [
        'slave1'=>[
            'type'     => 'MySQL',
            'host'     => '192.168.10.10',
            'user'     => 'homestead',
            'password' => 'secret',
            'dbname'   => 'test',
        ],
//        'slave2'=>[
//            'type'     => 'MySQL',
//            'host'     => '192.168.10.10',
//            'user'     => 'homestead',
//            'password' => 'secret',
//            'dbname'   => 'test2',
//        ],
    ],
];
